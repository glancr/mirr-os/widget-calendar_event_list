# frozen_string_literal: true

Rails.application.routes.draw do
  mount CalendarEventList::Engine => '/calendar_event_list'
end
