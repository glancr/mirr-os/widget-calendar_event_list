# frozen_string_literal: true

require 'json'

$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'calendar_event_list/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name = 'calendar_event_list'
  s.version = CalendarEventList::VERSION
  s.authors = ['Tobias Grasse']
  s.email = ['tg@glancr.de']
  s.homepage = 'https://glancr.de'
  s.summary = 'Widget for mirr.OS that displays calendar events.'
  s.description = 'Displays your calendar events from compatible sources.'
  s.license = 'MIT'
  s.metadata = { 'json' =>
                  {
                    type: 'widgets',
                    title: {
                      enGb: 'Calendar',
                      deDe: 'Kalender',
                      frFr: 'Calendrier',
                      esEs: 'Calendario',
                      plPl: 'Kalendarz',
                      koKr: '달력'
                    },
                    description: {
                      enGb: s.description,
                      deDe: 'Zeigt deine Kalender-Einträge aus kompatiblen Datenquellen.',
                      frFr: 'Affiche les événements de votre calendrier à partir de sources compatibles.',
                      esEs: 'Muestra tus eventos de calendario de fuentes compatibles.',
                      plPl: 'Wyświetla wydarzenia z kalendarza z kompatybilnych źródeł.',
                      koKr: '호환되는 출처에서 캘린더 일정을 표시합니다.'
                    },
                    sizes: [
                      {
                        w: 5,
                        h: 4
                      },
                      {
                        w: 5,
                        h: 6
                      },
                      {
                        w: 5,
                        h: 8
                      },
                      {
                        w: 8,
                        h: 4
                      },
                      {
                        w: 8,
                        h: 6
                      },
                      {
                        w: 8,
                        h: 8
                      },
                      {
                        w: 12,
                        h: 8
                      },
                      {
                        w: 12,
                        h: 12
                      }
                    ],
                    languages: %i[enGb deDe frFr esEs plPl koKr],
                    group: :calendar,
                    compatibility: '0.9.3'
                  }.to_json }

  s.files = Dir[
    '{app,config,db,lib}/**/*',
    'MIT-LICENSE',
    'Rakefile',
    'README.md'
  ]

  s.add_development_dependency 'rails'
  s.add_development_dependency 'rubocop', '~> 0.81'
  s.add_development_dependency 'rubocop-rails'
end
