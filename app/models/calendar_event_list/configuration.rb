# frozen_string_literal: true

module CalendarEventList
  class Configuration < WidgetInstanceConfiguration
    attribute :show_location, :boolean, default: false
    attribute :show_current_events, :boolean, default: false
    attribute :hour12, :boolean, default: false
    attribute :displaymode, :string, default: "amount"
    attribute :days_ahead, :integer, default: 5
    attribute :event_amount, :integer, default: 5

    validates :show_location, :show_current_events, :hour12, boolean: true
  end
end
