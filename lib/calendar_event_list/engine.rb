# frozen_string_literal: true

module CalendarEventList
  class Engine < ::Rails::Engine
    isolate_namespace CalendarEventList
    config.generators.api_only = true
  end
end
